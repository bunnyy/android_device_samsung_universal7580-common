# Graphics
lib/hw/gralloc.exynos5.so
lib/hw/hwcomposer.exynos5.so
lib/hw/memtrack.exynos5.so
lib/libcsc.so
lib/libexynosdisplay.so
lib/libexynosscaler.so
lib/libexynosgscaler.so
lib/libexynosutils.so
lib/libexynosv4l2.so
lib/libfimg.so
lib/libhdmi.so
lib/libhwcutils.so
lib/libmpp.so

# Mobicore
bin/mcDriverDaemon:vendor/bin/mcDriverDaemon
lib/libMcClient.so
lib/libMcRegistry.so

# OMX
lib/libExynosOMX_Core.so
lib/libExynosOMX_Resourcemanager.so
lib/libstagefrighthw.so
lib/omx/libOMX.Exynos.AVC.Decoder.so
lib/omx/libOMX.Exynos.AVC.Encoder.so
lib/omx/libOMX.Exynos.HEVC.Decoder.so
lib/omx/libOMX.Exynos.HEVC.Encoder.so
lib/omx/libOMX.Exynos.MPEG2.Decoder.so
lib/omx/libOMX.Exynos.MPEG4.Decoder.so
lib/omx/libOMX.Exynos.MPEG4.Encoder.so
lib/omx/libOMX.Exynos.VP8.Decoder.so
lib/omx/libOMX.Exynos.VP8.Encoder.so
lib/omx/libOMX.Exynos.VP9.Decoder.so